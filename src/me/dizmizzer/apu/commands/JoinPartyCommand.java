package me.dizmizzer.apu.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import me.dizmizzer.apu.Main;
import me.dizmizzer.apu.managers.DuelManager;
import me.dizmizzer.apu.managers.ItemSerialization;
import me.dizmizzer.apu.managers.StringManager;

public class JoinPartyCommand implements CommandExecutor {

	private Main plugin;
	public JoinPartyCommand(Main main) {
		this.plugin = main;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			
			//Get string config
			StringManager sm = new StringManager(plugin);
			FileConfiguration fc = sm.getStringConfig();
			
			
			final Player p = (Player) sender;
			if (args.length == 0) {
				p.sendMessage(toColor(fc.getString("notenoughargs")));

				return true;
			}
			
			if (args[0].equalsIgnoreCase("accept")) {
				if (args.length != 2) {

					return true;
				}
				Player targ = Bukkit.getPlayer(args[1]);
				
				if (p == targ) {
					return true;
				}
				if (targ == null) {
					p.sendMessage(toColor(fc.getString("emptyinvite").replaceAll("\\{Target}", args[1])).replaceAll("\\{0}", "'"));

					return true;
				}
				
				if (plugin.players.get(targ) == null || plugin.players.get(targ) != p) {
					p.sendMessage(toColor(fc.getString("emptyinvite").replaceAll("\\{Target}", args[1])).replaceAll("\\{0}", "'"));
					return true;
				}
				
				if (!(targ.isOnline())) {
					p.sendMessage(toColor(fc.getString("notonline").replaceAll("\\{Player}", args[1])));
					plugin.players.remove(targ);
					return true;
				}

				if (targ.isOnline()) {
					
					for (Player pl : plugin.players.keySet()) {
						if (plugin.players.get(pl) == p) {
							if (pl != targ) {
								plugin.players.remove(pl);
							}
						}
					}
					final DuelManager dm = new DuelManager(plugin);
					String arena = dm.findArena(p);
					if (arena == null) {
                        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                            public void run() {
        						dm.findArena(p);
        						return;
                            }
                    }, 20);

					}
					if (arena != null) {
						dm.teleportPlayers(arena, targ, p);
						return true;
					}
				}
			}
			else if (args[0].equalsIgnoreCase("openinventoryof")) {
				if (args.length != 2) {
					return true;
				}
				Inventory inv = ItemSerialization.fromBase64(plugin.inventories.get(args[1]));
				p.openInventory(inv);
			}
			else if (args[0].equalsIgnoreCase("deny")) {
				if (args.length != 2) {

					return true;
				}
				Player targ = Bukkit.getPlayer(args[1]);
				
				if (p == targ) {
					return true;
				}
				
				if (targ == null) {
					p.sendMessage(toColor(fc.getString("emptyinvite").replaceAll("\\{Target}", args[1])).replaceAll("\\{0}", "'"));
					return true;
				}
				if (!(targ.isOnline())) {
					p.sendMessage(toColor(fc.getString("notonline").replaceAll("\\{Target}", targ.getName())));
					return true;
				}
				if (plugin.players.get(targ) == null || plugin.players.get(targ) != p) {
					p.sendMessage(toColor(fc.getString("emptyinvite").replaceAll("\\{Target}", args[1])).replaceAll("\\{0}", "'"));
					return true;
				}

				else {
					targ.sendMessage(toColor(fc.getString("invitedenyreceive").replaceAll("\\{Target}", p.getName()).replaceAll("\\{0}", "'")));
					p.sendMessage(toColor(fc.getString("invitedenysend").replaceAll("\\{Sender}", p.getName()).replaceAll("\\{0}", "'")));
				}

			}
			else {
				
				Player target = Bukkit.getPlayer(args[0]);
				
				if (target == null) {
					p.sendMessage(toColor(fc.getString("notonline").replaceAll("\\{Target}", args[0])));
					return true;
				}
				
				if (target == p) {
					return true;
				}

				if (plugin.combatTime.containsKey(p)) {
					p.sendMessage(toColor(fc.getString("stillincombat").replaceAll("\\{Target} is", "You are")));
					return true;
				}
				if (plugin.combatTime.containsKey(target)) {
					p.sendMessage(toColor(fc.getString("stillincombat").replaceAll("\\{Target}", target.getName())));
					return true;
				}

				else if (!(target.isOnline())) {
					p.sendMessage(toColor(fc.getString("notonline").replaceAll("\\{Target}", target.getName())));
					return true;
				}
				else {
					plugin.players.put(p, target);
					target.sendMessage(toColor(fc.getString("invitereceive").replaceAll("\\{Sender}", p.getDisplayName())));
					p.sendMessage(toColor(fc.getString("invitesend").replaceAll("\\{Target}", target.getName())));
				}

			}
		}
		return true;
	}

	private String toColor(String string) {

		String returnable = ChatColor.translateAlternateColorCodes('&', string);
		return returnable;
	}

}
