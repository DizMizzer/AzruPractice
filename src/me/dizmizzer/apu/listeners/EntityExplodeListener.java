package me.dizmizzer.apu.listeners;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

import me.dizmizzer.apu.Main;

public class EntityExplodeListener implements Listener {
	
	private Main plugin;
	public EntityExplodeListener(Main main) {
		this.plugin = main;
	}
	
	@EventHandler
	public void onExplosion(EntityExplodeEvent e) {
		e.setCancelled(true);
		Location loc = e.getLocation();
		for (Player pla : plugin.getOnlinePlayers()) {
			
			if (pla.getLocation().distanceSquared(loc) <= 6*6) {
				pla.getWorld().createExplosion(pla.getLocation(), 0.0F, false);
				pla.damage(10.0);;

				}
			}	
		}

}
