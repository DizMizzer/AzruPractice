package me.dizmizzer.apu.listeners;

import java.io.FileNotFoundException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_7_R4.inventory.CraftInventoryCustom;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import me.dizmizzer.apu.Main;
import me.dizmizzer.apu.managers.StringManager;
import me.dizmizzer.apu.sql.WriteJson;
import net.minecraft.util.com.google.gson.JsonIOException;
import net.minecraft.util.com.google.gson.JsonSyntaxException;

public class InventoryClickListener implements Listener {
	
	private Main pl;
	public static String inventory;
	public static String armorinv;

	
	public InventoryClickListener(Main main) {
		this.pl = main;
		Bukkit.getServer().getPluginManager().registerEvents(this, pl);
	}
		
	
	@EventHandler
	public void onInvClick(InventoryClickEvent e) throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		
		if(e.getSlotType().equals(SlotType.OUTSIDE))return;
		Player clicker = (Player) e.getWhoClicked();
		
		StringManager sm = new StringManager(pl);
		FileConfiguration fc = sm.getStringConfig();

		
        if (!e.getInventory().getName().equalsIgnoreCase("Choose Kit")) return;
        if (e.getCurrentItem().getItemMeta() == null) return;
		WriteJson wj = new WriteJson(pl);
		e.setCancelled(true);
		if (e.getCurrentItem().getDurability() == 0) {
			wj.writeKit((Player) e.getWhoClicked(), e.getSlot() + 1);
			clicker.sendMessage(toColor(fc.getString("kitsave")));
		}
		
		
		if (e.getCurrentItem().getDurability() == 5) {
			wj.getUserInventoryData((Player) e.getWhoClicked(), e.getSlot() - 8);
			clicker.sendMessage(toColor(fc.getString("kitload")));
		}
		if (e.getCurrentItem().getDurability() == 14) {
			clicker.sendMessage(toColor(fc.getString("kitdelete")));
			}
		}
	
	private String toColor(String string) {
		String returnable = ChatColor.translateAlternateColorCodes('&', string);
		return returnable;
	}


	public Inventory getArmorInventory(PlayerInventory inventory) {
        ItemStack[] armor = inventory.getArmorContents();
        CraftInventoryCustom storage = new CraftInventoryCustom(null, armor.length);
 
        for (int i = 0; i < armor.length; i++) 
            storage.setItem(i, armor[i]);
        return storage;
    }

	


}
