package me.dizmizzer.apu.listeners;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.google.common.collect.Lists;

import me.dizmizzer.apu.Main;
import me.dizmizzer.apu.ScoreBoard;
import me.dizmizzer.apu.sql.WriteJson;

public class PlayerJoinListener implements Listener {
	
	public Main plugin;
	private HashMap<Player, Integer> kill;
	private HashMap<Player, Integer> death;

	private ArrayList<Player> notincombat;
	
	public PlayerJoinListener(Main main, HashMap<Player, Integer> kill, HashMap<Player, Integer> death, ArrayList<Player> player) {
		this.plugin = main;
		this.kill = kill;
		this.death = death;
		this.notincombat = player;
		Bukkit.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	private File JsonFile;

	@EventHandler
	public void onRespawn(PlayerRespawnEvent e) {
		WriteJson js = new WriteJson(plugin, kill, death);
		js.getUserInventoryData(e.getPlayer(), 1);
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		
		LeaveListener ll = new LeaveListener(plugin);
		try {
			ll.saveLocation(e.getPlayer().getLocation(), e.getPlayer());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		JsonFile = new File(plugin.getDataFolder() + "//"+ e.getPlayer().getUniqueId().toString() + ".json");
		
		WriteJson js = new WriteJson(plugin, kill, death);
		if (!(JsonFile.exists())) {
			js.addJsonUser(e.getPlayer());
			kill.put(e.getPlayer(), 0);
			death.put(e.getPlayer(), 0);
		}
		
		else {
			int kills = js.getUserKillsData(e.getPlayer());
			int deaths = js.getUserDeathsData(e.getPlayer());
			this.kill.put(e.getPlayer(), kills);
			this.death.put(e.getPlayer(), deaths);
		}
		

		ScoreBoard scoreboard = new ScoreBoard(plugin, kill, death);
		for (Player target : getOnlinePlayers()) {
			scoreboard.setScoreboardToPlayer(target);
		}
		scoreboard.setScoreboardToPlayer(e.getPlayer());
		
		if (notincombat.contains(e.getPlayer())) {
			js.getUserInventoryData(e.getPlayer(), 1);
			return;
		}
		else {
			e.getPlayer().getInventory().clear();
			js.getUserInventoryData(e.getPlayer(), 1);
		}
	}
	
	public static List<Player> getOnlinePlayers() {
	    List<Player> list = Lists.newArrayList();
	    for (World world : Bukkit.getWorlds()) {
	        list.addAll(world.getPlayers());
	    }
	    return Collections.unmodifiableList(list);
	}
}
