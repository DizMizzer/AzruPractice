package me.dizmizzer.apu.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;

import me.dizmizzer.apu.Main;
import me.dizmizzer.apu.inventories.Anvilinventory;

public class ClickAnvilListener implements Listener {
	
	private Main plugin;
	
	public ClickAnvilListener(Main main) {
		this.plugin = main;
		Bukkit.getServer().getPluginManager().registerEvents(this, plugin);
	}

	
	@EventHandler
	public void onBlockClick(PlayerInteractEvent e) {
		if (e.getAction() != Action.RIGHT_CLICK_BLOCK) {
			return;
		}
		if (e.getClickedBlock().getType() != Material.ANVIL) {
			return;
		}
		else {
			
		}
	}
	
	@EventHandler
	public void inventoryOpen(InventoryOpenEvent e) {
		if (e.getInventory().getType() == InventoryType.ANVIL) {
			e.setCancelled(true);
			Player player = (Player) e.getPlayer();
			Anvilinventory evente = new Anvilinventory();
			evente.openKitInventory(player);
		}
		
	}

}
