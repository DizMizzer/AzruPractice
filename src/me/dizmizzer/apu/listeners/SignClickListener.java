package me.dizmizzer.apu.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import me.dizmizzer.apu.Main;
import me.dizmizzer.apu.sql.WriteJson;

public class SignClickListener implements Listener {
	
	private Main plugin;
	public SignClickListener(Main main) {
		this.plugin = main;
		Bukkit.getServer().getPluginManager().registerEvents(this, main);
	}
	
	@EventHandler
	public void onSign(PlayerInteractEvent e) {
		if (e.getAction() != Action.RIGHT_CLICK_BLOCK) {
			return;
		}
		else {
			if (e.getClickedBlock().getType() == Material.SIGN) {
				Sign sign = (Sign) e.getClickedBlock().getState();
				if (sign.getLine(1).equalsIgnoreCase("[kit]") && sign.getLine(2).equalsIgnoreCase("default")) {
					WriteJson wj = new WriteJson(plugin);
					wj.getUserInventoryData(e.getPlayer(), 1);
				}
			}
		}
	}

}
