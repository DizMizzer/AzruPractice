package me.dizmizzer.apu.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.scheduler.BukkitRunnable;

import me.dizmizzer.apu.Main;
import me.dizmizzer.apu.managers.StringManager;

public class PlayerCombatListener implements Listener {
	
	private Main plugin;
	public PlayerCombatListener(Main main) {
		this.plugin = main;
		Bukkit.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void playerHit(EntityDamageByEntityEvent e) {
		StringManager sma = new StringManager(plugin);
		final FileConfiguration fc = sma.getStringConfig();
		if (!(e.getEntity() instanceof Player ) || !(e.getDamager() instanceof Player)) {
			return;
		}
		
		
		else {
			final Player hitter = (Player) e.getEntity();
			final Player damaged = (Player) e.getDamager();

			if (plugin.combatTime.containsKey(hitter.getName())) {
				plugin.combatTime.put(hitter.getName(), 60);
			}
			
			else {
				plugin.combatTime.put(hitter.getName(), 60);
				hitter.sendMessage(toColor(fc.getString("incombat")));
				plugin.combatTask.put(hitter.getName(), new BukkitRunnable() {
					
					String names = hitter.getName();
					Player ohitter = Bukkit.getPlayer(hitter.getName());
	                    public void run() {
	                    	plugin.combatTime.put(names, plugin.combatTime.get(names) - 1);
	                    	try {
	                            if (plugin.combatTime.get(names) == 0) {
	                            	
	                            	ohitter = Bukkit.getPlayer(names);
	                            	plugin.combatTime.remove(names);
	                            	plugin.combatTask.remove(names);
	                            	Bukkit.getLogger().info("DEbug");
	                            	if (!(ohitter.isOnline())) {
	                            	}
	                            	else {
	                    				hitter.sendMessage(toColor(fc.getString("outofcombat")));
	                            	}
                                    this.cancel();
	                            }
	                    	}
	                    	catch (NullPointerException e) {
                            	plugin.combatTime.remove(names);
                            	plugin.combatTask.remove(names);

                                this.cancel();

	                    	}
	                    }
	            });
	           
				plugin.combatTask.get(hitter.getName()).runTaskTimer(plugin, 20, 20);

			}
			if (plugin.combatTime.containsKey(damaged.getName())) {
				plugin.combatTime.put(damaged.getName(), 60);
			}
			else {
				plugin.combatTime.put(damaged.getName(), 60);
				damaged.sendMessage(toColor(fc.getString("incombat")));

				plugin.combatTask.put(damaged.getName(), new BukkitRunnable() {
	            	Player damagedo = Bukkit.getPlayer(damaged.getName());
	            	String name = damagedo.getName();
	                    public void run() {
	                    	plugin.combatTime.put(name, plugin.combatTime.get(name) - 1);
	                    	try {
	                    	if (plugin.combatTime.get(name) == 0) {
	                            	damagedo = Bukkit.getPlayer(name);
	                            	plugin.combatTime.remove(name);
	                            	plugin.combatTask.remove(name);
	                            	if (!(damagedo.isOnline())) {
	                            		
	                            	}
	                            	else {
	                    				damaged.sendMessage(toColor(fc.getString("outofcombat")));
	                            	}

	                                    this.cancel();
	                            }
	                    	}
	                    	catch (NullPointerException e) {
                            	plugin.combatTime.remove(name);
                            	plugin.combatTask.remove(name);

                                this.cancel();

	                    	}
	                    }
	            });
	           
				plugin.combatTask.get(damaged.getName()).runTaskTimer(plugin, 20, 20);

			}

		}
 	}
	
	private String toColor(String string) {

		String returnable = ChatColor.translateAlternateColorCodes('&', string);
		return returnable;
	}

	

}
