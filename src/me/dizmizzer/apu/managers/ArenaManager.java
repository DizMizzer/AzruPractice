package me.dizmizzer.apu.managers;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.Configuration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

import me.dizmizzer.apu.Main;

public class ArenaManager {
	
	private Main plugin;

	static Main pl;
	private HashMap<String, Location> arenaloc1 = new HashMap<String, Location>();
	private HashMap<String, Location> arenaloc2 = new HashMap<String, Location>();
	public HashMap<String, Location> arenatp1 = new HashMap<String, Location>();
	public HashMap<String, Location> arenatp2 = new HashMap<String, Location>();

	public HashMap<String, Boolean> available = new HashMap<String, Boolean>();
	

	static ArenaManager instance = new ArenaManager(pl);
	public static ArenaManager getInstance() {
		return instance;
		
	}
	private ArenaManager(Main main) {
		this.plugin = main;
	}
	public ArenaManager(Main main, Player player) {
		
		pl = main;
		this.plugin = main;
		Configuration config = SettingsManager.getConfig();
		Collection<String> arenalist = config.getConfigurationSection("maps").getKeys(false);
		
		for (String map : arenalist) {
			double x = config.getDouble("maps." + map + ".from.x");
			double y = config.getDouble("maps." + map + ".from.y");
			double z = config.getDouble("maps." + map + ".from.z");
			double x2 = config.getDouble("maps." + map + ".to.x");
			double y2 = config.getDouble("maps." + map + ".to.y");
			double z2 = config.getDouble("maps." + map + ".to.z");
			
			double x3 = config.getDouble("maps." + map + ".spawn1.x");
			double y3 = config.getDouble("maps." + map + ".spawn1.y");
			double z3 = config.getDouble("maps." + map + ".spawn1.z");
			double x4 = config.getDouble("maps." + map + ".spawn2.x");
			double y4 = config.getDouble("maps." + map + ".spawn2.y");
			double z4 = config.getDouble("maps." + map + ".spawn2.z");

			World w = player.getWorld();
			Location loc1 = new Location(w, x, y, z);
			Location loc2 = new Location(w, x2, y2, z2);
			Location cloc1 = new Location(w, x3, y3, z3);
			Location cloc2 = new Location(w, x4, y4, z4);
			
			if (main.parena.get(player) != null) {
				if ( main.parena.get(player).equals(map)) {
				}
			}
			else {
				available.put(map, true);
			}
			arenatp1.put(map, cloc1);
			arenatp2.put(map, cloc2);
			arenaloc2.put(map, loc2);
			arenaloc1.put(map, loc1);

		}
	}
	


	public void ArenaClean(Player player) {

		if (plugin.parena.keySet().size() < 1 || plugin.parena.get(player) == null) {
			player.sendMessage("WTF");
		}
		else {
			player.sendMessage(plugin.parena.get(player));
			player.sendMessage(arenaloc1.get(plugin.parena.get(player)).toString());
			player.sendMessage(arenaloc2.get(plugin.parena.get(player)).toString());
		}
		Location loc1 = arenaloc1.get(plugin.parena.get(player));
		Location loc2 = arenaloc2.get(plugin.parena.get(player));
		
		double x1 = 0;
		double x2 = 0;
		double y1 = 0;
		double y2 = 0;
		double z1 = 0; 
		double z2 = 0;
		if (loc1.getX() < loc2.getX()) {
			x1 = loc1.getX();
			x2 = loc2.getX();
		}
		else {
			x1 = loc2.getX();
			x2 = loc1.getX();
		}
		if (loc1.getY() < loc2.getY()) {
			y1 = loc1.getY();
			y2 = loc2.getY();
		}
		else {
			y1 = loc2.getY();
			y2 = loc1.getY();
		}
		if (loc1.getZ() < loc2.getZ()) {
			z1 = loc1.getZ();
			z2 = loc2.getZ();
		}
		else {
			z1 = loc2.getZ();
			z2 = loc1.getZ();
		}

		for (int i = (int) x1; i <= x2; i++) {
			for (int i1 = (int) y1; i1 <= y2; i1++) {
				for (int i2 = (int) z1; i2 <= z2; i2++) {
					Location loc = new Location(player.getWorld(), i, i1, i2);
					Material m = loc.getBlock().getType();
					if (m == Material.WATER || m == Material.LAVA || m == Material.STATIONARY_LAVA || m == Material.STATIONARY_WATER || m == Material.COBBLESTONE || m == Material.WOOD) {
						loc.getBlock().setType(Material.AIR);
					}
				}
			}
		}
		
		final Player pla = player;
		final double xx1 = x1;
		final double xx2 = x2;
		final double yy1 = y1;
		final double yy2 = y2;
		final double zz1 = z1;
		final double zz2 = z2;

		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			  public void run() {
					World world = pla.getWorld();
					List<Entity> entlist = world.getEntities();
					
					for (Entity ent : entlist) {
						Location loc = ent.getLocation();
						if (loc.getX() < xx1 || loc.getX() > xx2 || loc.getY() < yy1 || loc.getY() > yy2 || loc.getZ() < zz1 || loc.getZ() > zz2) {
							
						}
						else {
							if (ent instanceof Item) {
								ent.remove();
							}
						}
					}

			  }
			}, 5L);

	}

}
