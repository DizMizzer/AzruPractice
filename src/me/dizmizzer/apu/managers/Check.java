package me.dizmizzer.apu.managers;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;



public class Check {
	
	public void CheckWin(HashMap<Player, Integer> hash, Player p) {
		Material iih = p.getItemInHand().getType();
		int Kills = hash.get(p);
		if (Kills == 3) {
			p.getInventory().addItem(new ItemStack(Material.ENDER_PEARL, 3));
			Bukkit.broadcastMessage(ChatColor.YELLOW + p.getName() + " is now on a " + ChatColor.GOLD + hash.get(p) + ChatColor.YELLOW + " killstreak!");
		}
		if (Kills == 5) {
			p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 30*20, 1),true);
			Bukkit.broadcastMessage(ChatColor.YELLOW + p.getName() + " is now on a " + ChatColor.GOLD + hash.get(p) + ChatColor.YELLOW + " killstreak!");

		}
		if (Kills == 8) {
			p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 15*20, 0),true);
			Bukkit.broadcastMessage(ChatColor.YELLOW + p.getName() + " is now on a " + ChatColor.GOLD + hash.get(p) + ChatColor.YELLOW + " killstreak!");

		}
		if (Kills == 10) {
			Bukkit.broadcastMessage(ChatColor.YELLOW + p.getName() + " is now on a " + ChatColor.GOLD + hash.get(p) + ChatColor.YELLOW + " killstreak!");

			if (iih == Material.DIAMOND_SWORD|| iih == Material.WOOD_SWORD || iih == Material.GOLD_SWORD || iih == Material.STONE_SWORD || iih == Material.IRON_SWORD) {
				int el = p.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL);
				if (el < 4) {
					p.getItemInHand().addEnchantment(Enchantment.DAMAGE_ALL, el + 1);
				}
			}
		}
		if (Kills == 15) {
			Bukkit.broadcastMessage(ChatColor.YELLOW + p.getName() + " is now on a " + ChatColor.GOLD + hash.get(p) + ChatColor.YELLOW + " killstreak!");
			ItemStack FB = CreateItem(Material.BOW, ChatColor.RED + "" + ChatColor.ITALIC + "FlameBow");
			ItemMeta FBA = FB.getItemMeta();
			FBA.addEnchant(Enchantment.ARROW_DAMAGE, 3, true);
			FBA.addEnchant(Enchantment.ARROW_FIRE, 1, true);
			FB.setItemMeta(FBA);
			p.getInventory().addItem(FB);
		}
		if (Kills == 20) {
			Bukkit.broadcastMessage(ChatColor.YELLOW + p.getName() + " is now on a " + ChatColor.GOLD + hash.get(p) + ChatColor.YELLOW + " killstreak!");
			ItemStack FB = CreateItem(Material.DIAMOND_SWORD, ChatColor.RED + "" + ChatColor.ITALIC + "FireSword");
			ItemMeta FBA = FB.getItemMeta();
			FBA.addEnchant(Enchantment.DAMAGE_ALL, 2, true);
			FBA.addEnchant(Enchantment.FIRE_ASPECT, 1, true);
			FB.setItemMeta(FBA);
			p.getInventory().addItem(FB);
		}
		if (Kills > 24) {
			ItemStack GH = CreateItem(Material.GOLDEN_APPLE, ChatColor.LIGHT_PURPLE + "Golden Head");
			p.getInventory().addItem(GH);

		}
		if (Kills == 25) {
			Bukkit.broadcastMessage(ChatColor.YELLOW + p.getName() + " is now on a " + ChatColor.GOLD + hash.get(p) + ChatColor.YELLOW + " killstreak!");
		}
		if (Kills == 50) {
			Bukkit.broadcastMessage(ChatColor.YELLOW + p.getName() + " is now on a " + ChatColor.GOLD + hash.get(p) + ChatColor.YELLOW + " killstreak!");

			ItemStack PO = CreateItem2();
			p.getInventory().addItem(PO);
		}

	}
	private ItemStack CreateItem(Material m, String n) {
		
		ItemStack i = new ItemStack(m, 1);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(n);
		i.setItemMeta(im);
		return i;
		
	}

	private ItemStack CreateItem2() {
		ItemStack po = new Potion(PotionType.SPEED, 1).toItemStack(2);
		
		return po;
	}


}
