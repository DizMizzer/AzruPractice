package me.dizmizzer.apu.managers;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_7_R4.inventory.CraftInventoryCustom;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import me.dizmizzer.apu.Main;
import me.dizmizzer.apu.sql.WriteJson;

public class DuelManager {
	
	private Main plugin;
	
	public DuelManager(Main main) {
		
		
		this.plugin = main;
	}
	
	public String findArena(Player p1) {
		ArenaManager am = new ArenaManager(plugin, p1);

		for (String key : am.available.keySet()) {
			if (am.available.get(key)) {
				am.available.put(key, false);
				return key;
			}
		}
		return null;
	}
	public void teleportPlayers(String key, Player p1, Player p2) {
		if (p1 != null && p2 != null) {
			plugin.inGame.put(p1, true);
			plugin.inGame.put(p2, true);

			ArenaManager am = new ArenaManager(plugin, p1);
			plugin.parena.put(p1, key);
			plugin.parena.put(p2, key);

			Location loc1 = am.arenatp1.get(key);
			Location loc2 = am.arenatp2.get(key);
			p1.teleport(loc1);
			p2.teleport(loc2);
			WriteJson wj = new WriteJson(plugin, plugin.kills, plugin.deaths);

			wj.getUserInventoryData(p1, 1);
			wj.getUserInventoryData(p2, 1);

		}
	}
    public Inventory getArmorInventory(PlayerInventory inventory) {
        ItemStack[] armor = inventory.getArmorContents();
        CraftInventoryCustom storage = new CraftInventoryCustom(null, armor.length);
 
        for (int i = 0; i < armor.length; i++) 
            storage.setItem(i, armor[i]);
        return storage;
    }


}
